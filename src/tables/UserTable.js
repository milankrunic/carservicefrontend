import React from "react";

const UserTable = ({
        usersList,
        }) => {
        return (
            <>
            <div>
                <h1>Users</h1>
            </div>
            <div>
                <table border={1}>
                <thead>
                    <th className="th">ID</th>
                    <th className="th">First Name</th>
                    <th className="th">Last Name</th>
                    <th className="th">Gender</th>
                    <th className="th">Address</th>
                    <th className="th">Phone Number</th>
                    <th className="th">Username</th>
                </thead>
                {usersList.map((val) => {

                    return (
                    <tr>
                        <td>{val.idUser}</td>
                        <td>{val.firstName}</td>
                        <td>{val.lastName}</td>
                        <td>{val.gender}</td>
                        <td>{val.address}</td>
                        <td>{val.phoneNumber}</td>
                        <td>{val.username}</td>
                    </tr>
                    );
                })}
                </table>
            </div>
            </>
        );
};

export default UserTable;