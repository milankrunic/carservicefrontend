import './App.css';
// import Navigation from './components/Navigation';
import DataContextProvider from './context/DataContext';
import { Route, Routes } from "react-router-dom";
import Login from './pages/Login';
import AdministratorPage from './pages/AdministratorPage';
import RepairerPage from './pages/RepairerPage';
import CustomerPage from './pages/CustomerPage';

function App() {

  return (
    <div className="App">
      <DataContextProvider>
        {/* <Navigation /> */}
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/administrator-page" element={<AdministratorPage />} />
          <Route path="/repairer-page" element={<RepairerPage />} />
          <Route path="/customer-page" element={<CustomerPage />} />
        </Routes>
      </DataContextProvider>
    </div>
  );
}

export default App;
