import ListUsers from "../components/ListUsers";
import AddAdministratorComponent from "../components/addComponents/AddAdministratorComponent";
import AddRepairerComponent from "../components/addComponents/AddRepairerComponent";

const AdministratorPage = () => {

        return (
            <>
            <div>
                <h1>Welcome!</h1>
                <p>OVO JE STRANICA ZA ADMINA!</p>
                <ListUsers />
                <AddAdministratorComponent />
                <AddRepairerComponent />
            </div>

            </> 
        );
}; 

export default AdministratorPage;