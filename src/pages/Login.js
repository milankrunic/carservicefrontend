import React, { useState } from "react";
import AddCustomerComponent from "../components/addComponents/AddCustomerComponent";

const Login = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    function _decodeJWT(token) {
      try {
            var decodedData = JSON.parse(atob(token.split('.')[1]));
          
            console.log('Decoded JWT token:');
            console.log(decodedData);
            console.log("===========================================================================");
        
            return decodedData;
          } catch (e) {
            console.log('Error decoding JWT. JWT Token is null.');
            return null;
          }
    }

    function login() {
        const user = {
            username: username,
            password: password,
        };

        fetch("http://localhost:8080/api/user/login", {
            headers: {
                "Content-Type": "application/json",
            },
            method: "post",
            body: JSON.stringify(user),
        })
            .then((response) => {
                if(response.status === 200)
                    return Promise.all([response.json(), response.headers]);
                else
                    return Promise.reject("The login information is incorrect!");
            })
            .then(([body]) => {
                var jwt = body.value;
                localStorage.setItem('jwt', jwt);
                var decodedJWTData = _decodeJWT(jwt);
                var role;
                if (decodedJWTData != null) {
                  role = decodedJWTData.role;
                }
                if (role === "[ROLE_ADMINISTRATOR]"){
                  window.location.href = "administrator-page";
                }
                else if(role === "[ROLE_REPAIRER]"){
                  window.location.href = "repairer-page";
                }
                else{
                  window.location.href = "customer-page";
                }
            })
            .catch((message) => {
                alert(message);
            });
    }

        return (
            <>
            <div id="login">
              <div id="form">
                <h1 id="loginH1">Welcome!</h1>
                <h2 id="loginH2">Sing in</h2>
                  <form id="loginInput"> 
                    <div>
                      <div className="col-sm-10">
                        <input type="text" className="form-control" id="username"
                         placeholder="Username:" value={username} onChange={(e) => setUsername(e.target.value)}/>
                      </div>
                    </div>
                    <div>
                      <div className="col-sm-10">
                        <input type="password" className="form-control" id="password" 
                         placeholder="Password:"value={password} onChange={(e) => setPassword(e.target.value)}/>
                      </div>
                    </div>
                    
                    <div id="loginBTNS">
                      <div>
                        <input className="btn btn-primary" onClick={() => login()} type="button" id="loginBTN" value="Login"/>
                      </div>
                      <div>
                        <AddCustomerComponent />     
                      </div>
                    </div>
                  </form>
              </div>
            </div>
            </>
        );
};

export default Login;