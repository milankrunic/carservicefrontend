import "../App.css";
import React from "react";
import { NavLink } from "react-router-dom";

const navLinks = [
//   { to: "/", name: "ListUsers" },
//   { to: "/income-groups", name: "Income groups" },
//   { to: "/expense-groups", name: "Expense groups" },
];

const Navigation = () => {
  let activeStyle = {
    fontWeight: "bold",
    color: "black",
  };

  return (
    <>
      <nav variant="pills" defaultActiveKey="/">
        <h1>Car Service</h1>
        <ul>
          <li>
            {navLinks.map((navLink, index) => (
              <NavLink
                key={index}
                to={navLink.to}
                className="href"
                style={({ isActive }) => (isActive ? activeStyle : undefined)}
              >
                {navLink.name}
              </NavLink>
            ))}
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Navigation;