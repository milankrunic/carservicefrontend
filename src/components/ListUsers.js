import React, { useContext, useEffect } from "react";
import { DataContext } from "../context/DataContext";
import "../App.css";
import UserTable from "../tables/UserTable";

const ListUsers = () => {
  const {
    usersList,
    setUsersList,
    hasDataChanged,
  } = useContext(DataContext);

  useEffect(() => {
    const getUsers = async () => {
      fetch("http://localhost:8080/api/user", {
        headers: {
          "Content-Type": "Application/json", 'X-Auth-Token': localStorage.getItem('jwt'),
        }
        })
        .then((res) => {
          if (!res.ok) {
            throw new Error("Not a good status code");
          }
          return res.json();
        })
        .then((data) => {
            setUsersList(data);
        })
        .catch((e) => {
          console.log("Error: ", e);
        });
    };
    getUsers();
  }, [hasDataChanged, setUsersList]);


  return (
    <>
      <UserTable
        usersList={usersList}
      />
    </>
  );
};

export default ListUsers;