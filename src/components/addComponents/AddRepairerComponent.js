import "../../App.css";
import React, { useContext, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { DataContext } from "../../context/DataContext";

const AddRepairerComponent = () => {
  
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [gender, setGender] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [salary, setSalary] = useState("");
    const [specialization, setSpecialization] = useState("");

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const { hasDataChanged, setHasDataChanged } = useContext(DataContext);

  const AddRepairer = async () => {
    let alertMessage;

    if (!firstName) {
      alertMessage = "The first name field is empty!";
    } else if (!lastName) {
      alertMessage = "The last name field is empty!";
    } else if (!gender) {
        alertMessage = "The gender field is empty!";
    }else if (!address) {
        alertMessage = "The address field is empty!";
    }else if (!phoneNumber) {
        alertMessage = "The phone number field is empty!";
    }else if (!username) {
        alertMessage = "The username field is empty!";
    }else if (!password) {
        alertMessage = "The password field is empty!";
    }else if (!salary) {
        alertMessage = "The salary field is empty!";
    }else if (!specialization) {
        alertMessage = "The specialization field is empty!";
    }else {
        alertMessage = "Repairer is created!";
    }
        alertMessage && alert(alertMessage);

    let result = await fetch("http://localhost:8080/api/user/add_repairer", {
      method: "POST",
      body: JSON.stringify({ firstName, lastName, gender, address, phoneNumber, username, password, salary, specialization }),
      headers: {
        "Content-Type": "Application/json", 'X-Auth-Token': localStorage.getItem('jwt'),
      },
    });

    result = await result.json();
    if (result) {
      handleClose();
      setHasDataChanged(!hasDataChanged);
      setFirstName("");
      setLastName("");
      setGender("");
      setAddress("");
      setPhoneNumber("");
      setUsername("");
      setPassword("");
      setSalary("");
      setSpecialization("");
    }
  };

  return (
    <>
      <Button className="addButtonE" onClick={handleShow}>
        Add Repairer
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Repairer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={AddRepairer}>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                className="form-control"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                className="form-control"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Gender</Form.Label>
                <Form.Select 
                    aria-label="Default select example"
                    value={gender}
                    onChange={(e) => setGender(e.target.value)}>
                    <option></option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Address</Form.Label>
              <Form.Control
                type="text"
                className="form-control"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                type="number"
                className="form-control"
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Salary</Form.Label>
              <Form.Control
                type="number"
                className="form-control"
                value={salary}
                onChange={(e) => setSalary(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Specialization</Form.Label>
                <Form.Select 
                    aria-label="Default select example"
                    value={specialization}
                    onChange={(e) => setSpecialization(e.target.value)}>
                    <option></option>
                    <option value="Auto_mechanic">Auto_mechanic</option>
                    <option value="Car_electrician">Car_electrician</option>
                    <option value="Vulcanizer">Vulcanizer</option>
                    <option value="Tinsmith">Tinsmith</option>
                </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                className="form-control"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                className="form-control"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            <Button className="saveBTN" variant="primary" type="submit">
              Save
            </Button>
            <Button type="button" variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default AddRepairerComponent;
