import React, { createContext, useState } from "react";

export const DataContext = createContext();

const DataContextProvider = (props) => {
  const [usersList, setUsersList] = useState([]);

  const [hasDataChanged, setHasDataChanged] = useState(false);

  return (
    <DataContext.Provider
      value={{
        usersList,
        setUsersList,
        hasDataChanged,
        setHasDataChanged,
      }}
    >
      {props.children}
    </DataContext.Provider>
  );
};

export default DataContextProvider;